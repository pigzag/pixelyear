Syntax for use

The whole thing needs to go in a pair of:
''<pixelyear>
</pixelyear>''
tags.

Between the tags is a list dates with a month, day and 'pixel' separated with a pipe character "|" in the format month|day|pixel. The pixel is a number 1-5 where 1 is bad, 5 is good.

Example:
<pixelyear>
01|01|1
01|02|2
01|03|3
01|04|4
01|05|5
</pixelyear>

Oddly I've found month first makes more sense, even though I usually like dates the opposite way around.
