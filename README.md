pixelyear Plugin for DokuWiki

Outputs a colour coded chart displaying progess throughout a year

**This is extremely experimental.** *I have absolutely no idea what
I am doing. Even if this looks like it is doing what I want it to,
I am nowhere near certain that it is doing it in the right way and
that it isn't doing stuff that it shouldn't be.*
 
All documentation for this plugin can be found at
https://gitlab.com/pigzag/pixelyear

If you install this plugin manually, make sure it is installed in
lib/plugins/pixelyear/ - if the folder is called different it
will not work!

Please refer to http://www.dokuwiki.org/plugins for additional info
on how to install plugins in DokuWiki.

----
Copyright (C) Paul Brownsea <pigzag@gmx.co.uk>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See the LICENSING file for details
