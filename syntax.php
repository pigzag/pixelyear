  <?php
  /**
   * DokuWiki Plugin pixelyear (Syntax Component)
   *
   * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
   * @author  Paul Brownsea <pigzag@gmx.co.uk>
   */

   // must be run within Dokuwiki
   if(!defined('DOKU_INC')) die();

   /**
    * All DokuWiki plugins to extend the parser/rendering mechanism
    * need to inherit from this class
    */
   class syntax_plugin_pixelyear extends DokuWiki_Syntax_Plugin {

       public function getType(){ return 'formatting'; }
       public function getAllowedTypes() { return array('formatting', 'substition', 'disabled'); }
       public function getSort(){ return 158; }
       public function connectTo($mode) { $this->Lexer->addEntryPattern('<pixelyear.*?>(?=.*?</pixelyear>)',$mode,'plugin_pixelyear'); }
       public function postConnect() { $this->Lexer->addExitPattern('</pixelyear>','plugin_pixelyear'); }


       /**
        * Handle the match
        */
       public function handle($match, $state, $pos, Doku_Handler $handler){
           switch ($state) {
             case DOKU_LEXER_ENTER :
                  return array($state, '');
             case DOKU_LEXER_UNMATCHED :
                  return array($state, $match);
             case DOKU_LEXER_EXIT :
                  return array($state, '');
           }
           return array();
       }

       /**
        * Create output
        */
       public function render($mode, Doku_Renderer $renderer, $data) {
           // $data is what the function handle() return'ed.
           if($mode == 'xhtml'){
               /** @var Doku_Renderer_xhtml $renderer */
               list($state,$match) = $data;
               switch ($state) {
                   case DOKU_LEXER_ENTER :
                       $renderer->doc .= "<table>";
                       break;

                   case DOKU_LEXER_UNMATCHED :
                       $daysinmonth=array(
                         1 =>31,
                         2 =>28,
                         3 =>31,
                         4 =>30,
                         5 =>31,
                         6 =>30,
                         7 =>31,
                         8 =>31,
                         9 =>30,
                         10 =>31,
                         11 =>30,
                         12 =>31,
                       );
                       $colours = array (
                         0 => 'silver',
                         1 => 'red',
                         2 => 'orange',
                         3 => 'yellow',
                         4 => 'lime',
                         5 => 'green'
                       );
                       $rows = explode ("\n", $match);
                       $dates = array();
                       foreach ($rows as $row) {
                         list ($month, $day, $pixel) = explode ("|", $row);
                         if ($month>=1&&$month<=12) {
                           if ($day>=1&&$day<=31) {
                             if ($pixel>=1 and $pixel<=5) {
                               $dates[intval($month)."-".intval($day)]=$pixel;
                             }
                           }
                         }
                       }
                       for ($m=1;$m<=12;$m++){
                         $renderer->doc .= "<tr>";
                         for ($d=1;$d<=$daysinmonth[$m];$d++){
                           if (isset ($dates[$m."-".$d])) {
                             $colour = $colours [$dates[$m."-".$d]];
                           } else {
                             $colour = "silver";
                           }
                           $renderer->doc .= "<td style='background-color:{$colour}'>&nbsp;</td>";
                         }
                         $renderer->doc .= "</tr>";
                       }
                       break;
                   case DOKU_LEXER_EXIT :
                       $renderer->doc .= "</table>";
                       break;
               }
               return true;
           }
           return false;
       }
   }
